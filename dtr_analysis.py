#!/usr/bin/env python3
"""
analysis of data from completed renders

callable functions from this file:

    create_heatmap
    display_basic_render_info

------------------------------------------------------------------------------
Copyright 2015-2017 Alan Taylor

This file is part of dtr.

dtr is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

dtr is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with dtr.  If not, see <http://www.gnu.org/licenses/>.
------------------------------------------------------------------------------
"""

import copy                     # deepcopy
import datetime                 # timedelta
import statistics               # median
import time                     # time

from PIL import Image           # new
from PIL import ImageDraw       # Draw

import dtr_file_io as fio       # render_data_write
import dtr_utils as utils       # render_block_pillow


##############################################################################
# print information for user
##############################################################################

def display_basic_render_info(scr_start, time_taken_previous_renders, checked):
    """
    display the number of blocks each node completed, and the mean time it
    took it to complete its nodes

    --------------------------------------------------------------------------
    args
        scr_start : float
            script start time
        time_taken_previous_renders : float
            this will be zero if the render has not been restarted or
            will represent the accumulated time of previous runs of the
            script for this distributed render
        checked : list of lists
            [[x, y, z], ...]
            where:
                x = string, ip address
                y = int, block number
                z = int, time it took to render block in seconds
    --------------------------------------------------------------------------
    returns : none
    --------------------------------------------------------------------------
    """
    # assemble summary of render in data
    # [[ip_address, total_blocks_rendered, total_render_time], ...]
    seen = set()
    data = list()
    for blk_info in checked:
        if blk_info[0] not in seen:
            data.append([blk_info[0], 1, blk_info[2]])
            seen.add(blk_info[0])
        else:
            blk_summary = next(x for x in data if x[0] == blk_info[0])
            blk_summary[2] += blk_info[2]
            blk_summary[1] += 1

    # generate mean block render times
    for blk_summary in data:
        blk_summary[2] //= blk_summary[1]

    script_end = time.time()
    real_time = script_end - scr_start + time_taken_previous_renders
    print('\n'
          + 'node'.rjust(16)
          + 'blocks'.rjust(8)
          + 'mean duration'.rjust(20))

    # sort by highest number of blocks first (inner),
    # then by lowest mean render time (outer)
    data = sorted(sorted(data, key=lambda x: x[1], reverse=True), key=lambda x: x[2])

    # summary table
    for i in data:
        print(
            i[0].rjust(16) + str(i[1]).rjust(8) +
            str(datetime.timedelta(seconds=int(i[2]))).rjust(20))

    print(
        '\n' + 'real time'.rjust(44) + '\n' +
        str(datetime.timedelta(seconds=int(real_time))).rjust(44))

    # calculate a realistic speed-up ratio: compare the real time total to the
    # time it would take the single fastest node to render all the blocks
    if len(data) > 1:
        best_single_time = min(data, key=lambda x: x[2])[2] * len(checked)
        perf_gain = ((best_single_time - real_time) / best_single_time) * 100

        print('\n' + 'performance gain'.rjust(44))
        print((format(perf_gain, '.1f') + '%').rjust(44))


##############################################################################
# heatmap
##############################################################################

def _blocks_by_node(blocks):
    """
    take a list of block data, and return it separated (the block numbers
    are discarded), for example:

    [['10.0.0.1', 1, 173], ['10.0.0.2', 2, 140], ['10.0.0.1', 3, 151],
     ['10.0.0.2', 4, 160]]

    becomes:

    ['10.0.0.1', '10.0.0.2']    (a set of unique node ip addresses)
    and
    [[173, 151], [140, 160]]    (the matching render times)

    --------------------------------------------------------------------------
    args
        blocks : list of lists [[a, b, c], ...]
            where:
            a = string, ip address
            b = int, block number rendered
            c = int, render time in seconds
    --------------------------------------------------------------------------
    returns
        nodes : list of strings
        times : list containing lists of ints (render times in seconds)
    --------------------------------------------------------------------------
    """
    times = list()
    nodes = list()
    for block in blocks:
        if block[0] not in nodes:
            nodes.append(block[0])
            times.append([block[2]])
        else:
            times[nodes.index(block[0])].append(block[2])

    return nodes, times


def _time_to_greyscale(blocks):
    """
    convert render times to greyscale pixel values, where black (0, 0, 0),
    and white (255, 255, 255), represent the fastest and slowest block
    render times respectively

    --------------------------------------------------------------------------
    args
        blocks : list of lists [[a, b, c], ...]
            where:
            a = string, ip address
            b = int, block number rendered
            c = int, render time in seconds
    --------------------------------------------------------------------------
    returns : none
        no explicit return, mutable type (blocks) amended in place
    --------------------------------------------------------------------------
    """
    # scale time values to between 0 and 255 (greyscale pixel values)
    render_times = [x[2] for x in blocks]
    t_min, t_max = min(render_times), max(render_times)

    if t_max != t_min:
        scale = 255 / (t_max - t_min)
    else:
        scale = 0

    for block in blocks:
        block[2] = int((block[2] - t_min) * scale)


def create_heatmap(settings, blocks):
    """
    produce a heatmap for the rendered image, where the brightest colours
    represent the longest render times.  The resolution of the heatmap will
    match the resolution of the original rendered image.

    efforts are made to adjust for differences in node performance

    --------------------------------------------------------------------------
    args
        settings : image_config dictionary
            contains core information about the image to be rendered
        blocks : list of lists [[x, y, z], ...]
                where:
                x = string, ip address
                y = int, block number rendered
                z = int, render time in seconds
    --------------------------------------------------------------------------
    returns : none
        heatmap PNG file is written to the filesystem
    --------------------------------------------------------------------------
    """
    if not settings['heatmap']:
        return

    # store data to disk, it isn't used in the analysis but sometimes it's
    # useful to be able to inspect the raw data
    fio.render_data_write((settings, blocks))

    print('>> writing heatmap to: heatmap.png')

    local_blocks = copy.deepcopy(blocks)

    # obtain median render times for all nodes
    nodes, times = _blocks_by_node(local_blocks)
    medians = [statistics.median(t) for t in times]

    # compensate for differences in performance between nodes
    med = max(medians)
    adjust = {n: m / med for n, m in zip(nodes, medians)}
    for block in local_blocks:
        block[2] *= adjust[block[0]]

    # scale time values to between 0 and 255 (greyscale pixel values)
    _time_to_greyscale(local_blocks)

    # draw
    hmap = Image.new('L', (settings['image_x'], settings['image_y']))
    draw = ImageDraw.Draw(hmap)
    for block in local_blocks:
        x_min, y_min, x_max, y_max = utils.render_block_pillow(settings, block[1])
        draw.rectangle((x_min, y_min, x_max, y_max), fill=block[2], outline=None)

    # save
    hmap.save('heatmap.png', 'PNG')


##############################################################################
def main():
    """
    if the user has directly run this file, reload the render data from disk
    and recreate the heatmap

    the purpose of this is to handle the case where the user has seen an
    obvious outlier in the heatmap. they then have the opportunity to inspect
    the data interactively (in iPython for example), handle the outlier, and
    recreate the heatmap.

    for example on macOS outliers might be caused by spotlight indexing which
    could significantly slow the rendering of a single block, skewing the
    entire heat map.

    a contrived example:

    $ ipython

    In [1]: import dtr_file_io as fio
    In [2]: image_config, checked = fio.render_data_read()
    in [3]: checked
    Out[3]:
    [['127.0.0.1', 1, 13],
     ['127.0.0.1', 2, 12],
     ['127.0.0.1', 3, 72],
     ['127.0.0.1', 4, 11]]
    in [4]: ind = checked.index(['127.0.0.1', 3, 72])
    in [5]: checked[ind] = ['127.0.0.1', 3, 12]
    In [6]: fio.render_data_write((image_config, checked))

    then manually recreate the heatmap by running:

    $ ./dtr_analysis.py && open heatmap.png
    """
    image_config, checked = fio.render_data_read()
    image_config['heatmap'] = True

    create_heatmap(image_config, checked)


##############################################################################
if __name__ == '__main__':
    main()
