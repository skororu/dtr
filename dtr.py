#!/usr/bin/env python3
"""
Distributed single frame render for Blender Cycles

do not set multiprocessing.set_start_method() and let default behaviour
prevail. this will use 'fork' on non-Windows platforms, and only use 'spawn'
(with its slow process startup characteristics) on Windows where 'fork' isn't
available.

------------------------------------------------------------------------------
Copyright 2015-2017 Alan Taylor

This file is part of dtr.

dtr is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

dtr is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with dtr.  If not, see <http://www.gnu.org/licenses/>.
------------------------------------------------------------------------------
"""

import math                     # ceil, log
import multiprocessing as mp    # Process, Queue
import os                       # path.isfile
import queue                    # Empty exception
import subprocess               # DEVNULL, Popen
import sys                      # exit, version
import time                     # sleep, time

import dtr_analysis as ana      # create_heatmap, display_basic_render_info
import dtr_benchmark as bm      # display_bench_cache
import dtr_data_struct as ds    # FILENAME_CONFIG_BLENDER
                                # FILENAME_FOR_RENDER
                                # FILENAME_RESTART_CONFIG
                                # FILENAME_RESTART_PROGRESS
                                # Production, UserExit
import dtr_file_io as fio       # backup_render,
                                # create_final_image_from_blocks,
                                # delete_blocks, distribute_files_to_node,
                                # progress_write, tidy_up_temporary_files
import dtr_init as init         # check_arguments, display_render_details,
                                # normal_start, restart_interrupted_render
import dtr_utils as utils       # block_render_filename, check_python_version
                                # despatch_order, node_alive,
                                # package_filename_for_cygwin


##############################################################################
# process support
##############################################################################

def replace_reorder(image_config, pipeline, block_num):
    """
    in the cases where the block render itself could not be completed or we
    could not fetch a completed block from a node - both usually occur because
    of network outages or a node otherwise going down - the salvaged
    block number would be rendered out of order leaving a preview image with
    a missing block (most probably) in the region of interest.

    this function reorders the blocks; due to the nature of the failure,
    this function should be called very rarely
    --------------------------------------------------------------------------
    args
        image_config : image_config dictionary
            contains core information about the image to be rendered
        pipeline : instance of class Production
            contains all the queues through which the production pipeline
            processes communicate
        block_num : int
            block number to be rendered again
    --------------------------------------------------------------------------
    returns : none
    --------------------------------------------------------------------------
    """
    with pipeline.lock:
        # empty the queue into local storage
        blocks_to_render = set()
        while not pipeline.blocks.empty():
            blocks_to_render.add(pipeline.blocks.get())

        # place failed block into set
        blocks_to_render.add(block_num)

        # set the despatch order given by the user
        blocks = list(blocks_to_render)
        utils.despatch_order(image_config, blocks)

        # refill the queue
        for block in blocks:
            pipeline.blocks.put(block)


def collect_block_from_node(image_config, block_info):
    """
    return true if the file was read with no errors

    scp options:
    -q    quiet mode
    --------------------------------------------------------------------------
    args
        image_config : image_config dictionary
            contains core information about the image to be rendered
        block_info : list [x, y, z]
            where:
            x = instance of RenderNode, the node we are collecting from
            y = int, block number
            z = int, time it took to render block in seconds
    --------------------------------------------------------------------------
    returns
        bool
            true if the block could be retrieved, false otherwise
    --------------------------------------------------------------------------
    """
    remote_filename = block_info[0].username + '@' + block_info[0].ip_address + ':' + \
        utils.block_render_filename(image_config, block_info[1])

    with subprocess.Popen(['scp', '-q', remote_filename, '.'],
                          stdout=subprocess.DEVNULL,
                          stderr=subprocess.DEVNULL) as proc:
        proc.wait()

    return proc.returncode == 0


def render_block(image_config, node, block):
    """
    render a block on a remote node

    run asynchronously using multiprocessing.Process from
    asynchronous_render()

    ---

    This function maintains long running ssh sessions, and some specific
    behaviour has been noticed during network outages:

    (1) The network can disconnect for short periods (for example, a few
    minutes) without any effect on performance.

    (2) If the network disconnects for a long period of time however
    (for example, over 20 minutes), proc.wait() may block permanently
    and the render node concerned will receive no further blocks to render
    until the script is halted then restarted, even if connectivity is
    restored.

    ---

    For the case of (2) above, this behaviour can be modified by the use of
    ServerAliveInterval and ServerAliveCountMax to allow ssh to exit cleanly
    before the point is reached when proc.wait() blocks permanently.
    See the man page for ssh_config for further information.

    However, such early termination means that it is probable that the existing
    Blender process will remain running on the remote node, and it will still
    be running when the next block is sent to the node.  Two concurrent renders
    running on the same node will not interfere with each other, but the
    initial render process will produce no useful output, and will simply slow
    down the new render process.

    This negative effect can be sidestepped using 'ssh -t' which causes ssh to
    kill the remote blender render on exit, but this consistently messes up
    the terminal configuration, stopping carriage returns being echoed.
    This effect can be corrected with 'stty sane', but this is not effective
    in all cases.

    --------------------------------------------------------------------------
    args
        image_config : image_config dictionary
            contains core information about the image to be rendered
        node : RenderNode instance
            contains information about the node we are checking
        block : int
            the number of the block to be rendered
    --------------------------------------------------------------------------
    returns
        bool
            True if the render completed correctly, False otherwise
    --------------------------------------------------------------------------
    """
    on_this_node = node.username + '@' + node.ip_address
    block_number_padded = str(block).zfill(image_config['padding'])

    filename_render = \
        utils.package_filename_for_cygwin(node.opsys, ds.FILENAME_FOR_RENDER)
    filename_python = \
        utils.package_filename_for_cygwin(node.opsys, ds.FILENAME_CONFIG_BLENDER
                                          + block_number_padded + '.py')

    render_the_block = node.binloc + ' -b ' + filename_render + \
        ' --python ' + filename_python

    # force exit approximately 6 minutes after the network has gone down,
    # this will NOT kill the Blender instance running on the remote node,
    # even when the network outage ends
    with subprocess.Popen(['ssh',
                           '-o', 'ServerAliveInterval=360',
                           '-o', 'ServerAliveCountMax=1',
                           on_this_node, render_the_block],
                          stdout=subprocess.DEVNULL,
                          stderr=subprocess.DEVNULL) as proc:
        proc.wait()

    return proc.returncode == 0


##############################################################################
# start up and shut down processes
##############################################################################

def start_processes(nodes, image_config, rpipe):
    """
    start up processes

    --------------------------------------------------------------------------
    args
        nodes : list of RenderNode instances
            contains information about all the nodes in the cluster
        image_config : image_config dictionary
            contains core information about the image to be rendered
        rpipe : instance of class Production
            contains all the queues through which the production pipeline
            processes communicate
    --------------------------------------------------------------------------
    returns
        parfun : list of multiprocessing.context.Process
            references for all render and collect processes
        info : multiprocessing.context.Process
            reference for message process
    --------------------------------------------------------------------------
    """

    # start up a message display process
    info = mp.Process(target=message, args=(rpipe,))
    info.start()

    # initialise a render process for each node
    parfun = []
    for node in nodes:
        parfun.append(mp.Process(target=render, args=(image_config, node, rpipe)))

    # initialise an appropriate number of collect processes
    num_nodes = len(nodes)
    num_processes = 1 if num_nodes < 2 else math.ceil(math.log(num_nodes))
    for _ in range(num_processes):
        parfun.append(mp.Process(target=collect, args=(image_config, rpipe)))

    # start up render and collect processes
    for pfu in parfun:
        pfu.start()

    return parfun, info


def stop_processes(rpipe, parfun, info):
    """
    shut down processes

    --------------------------------------------------------------------------
    args
        rpipe : instance of class Production
            contains all the queues through which the production pipeline
            processes communicate
        parfun : list of multiprocessing.context.Process
            references for all render and collect processes
        info : multiprocessing.context.Process
            reference for message process
    --------------------------------------------------------------------------
    returns : none
    --------------------------------------------------------------------------
    """
    # indicate to render and collect processes that they can terminate now
    rpipe.terminate.put(True)
    for pfu in parfun:
        pfu.join()

    # indicate to the info process that it can terminate now
    rpipe.terminate_info.put(True)
    info.join()


##############################################################################
# processes
##############################################################################

def render(image_config, node, pipeline):
    """
    despatches blocks to be rendered on a given node

    checks the node is responsive before attemping to despatch block

    it is a very rare case that rendering the block will fail (the render
    process may have manually been killed on the remote node for some reason),
    in this case the block being rendered will be returned to the queue.
    as multiprocessing.queue is a FIFO, this block will be rendered last

    it is also a rare case that the call to node_alive will find the node down
    simply because the code spends the vast majority of its time elsewhere
    --------------------------------------------------------------------------
    args
        image_config : image_config dictionary
            contains core information about the image to be rendered
        node : RenderNode instance
            contains information about the node we are checking
        pipeline : instance of class Production
            contains all the queues through which the production pipeline
            processes communicate
    --------------------------------------------------------------------------
    returns : none
        no explicit return, data transfer via pipeline queues
    --------------------------------------------------------------------------
    """
    # exit cleanly if anything is placed on the terminate queue
    while pipeline.terminate.empty():

        # make sure node is responsive before attemping to despatch a block
        if utils.node_alive(node):

            # get free block and despatch it for rendering
            try:
                with pipeline.lock:
                    block_num = pipeline.blocks.get(timeout=1)
            except queue.Empty:
                continue
            else:
                block_num_padded = str(block_num).rjust(image_config['padding'])
                pipeline.inform.put('block ' + block_num_padded
                                    + ' issued to node ' + node.ip_address)
                fio.distribute_files_to_node(image_config, node, block_num)
                t_start = time.time()
                if render_block(image_config, node, block_num):
                    render_duration = int(time.time() - t_start)
                    pipeline.collect.put([node, block_num, render_duration])
                else:
                    # handle rare failure mode gracefully
                    replace_reorder(image_config, pipeline, block_num)
                    pipeline.inform.put('problem completing render of block '
                                        + str(block_num) + ' on node '
                                        + node.ip_address +
                                        ' - it will be rendered again')
        else:
            # the node was unresponsive last time it was checked,
            # so wait a while before checking again
            time.sleep(10)


def collect(image_config, pipeline):
    """
    collect rendered blocks from nodes

    collection typically happens at the same time as the node is starting to
    build the scene for its next block render, a phase of the render process
    that does not typically fully utilise a multicore processor.

    --------------------------------------------------------------------------
    args
        image_config : image_config dictionary
            contains core information about the image to be rendered
        pipeline : instance of class Production
            contains all the queues through which the production pipeline
            processes communicate
    --------------------------------------------------------------------------
    returns : none
        no explicit return, data transfer via pipeline queues
    --------------------------------------------------------------------------
    """
    # exit cleanly if anything is placed on the terminate queue
    while pipeline.terminate.empty():
        try:
            block_info = pipeline.collect.get(timeout=1)
        except queue.Empty:
            continue
        else:
            block_num_padded = str(block_info[1]).rjust(image_config['padding'])
            nodeip = block_info[0].ip_address
            if collect_block_from_node(image_config, block_info):
                pipeline.check.put([nodeip, block_info[1], block_info[2]])
                pipeline.inform.put('block ' + block_num_padded + ' retrieved from ' + nodeip)
            else:
                # handle rare failure mode gracefully
                replace_reorder(image_config, pipeline, block_info[1])
                pipeline.inform.put('block ' + block_num_padded
                                    + ' could not be retrieved from ' + nodeip
                                    + ' and will rendered again')


def message(pipeline):
    """
    print messages for user

    other processes put items on pipeline.inform, this is the only function that
    gets items from it. make sure when the terminate signal is received, we
    do not quit if there are still pending messages on pipeline.inform.

    --------------------------------------------------------------------------
    args
        pipeline : instance of class Production
            contains all the queues through which the production pipeline
            processes communicate
    --------------------------------------------------------------------------
    returns : none
    --------------------------------------------------------------------------
    """
    while pipeline.terminate_info.empty() or not pipeline.inform.empty():
        try:
            info = pipeline.inform.get(timeout=1)
        except queue.Empty:
            continue
        else:
            print(info)


##############################################################################
# startup
##############################################################################

def main():
    """
    distributed render

    processes are used for render() as at least one process will be CPU bound
    if we are rendering on the machine this script is running on.

    processes are also used for collect() as tests using threads showed slow
    (though still successful) recovery from temporary network disruptions
    """
    script_start = time.time()

    # basic configuration data for image render, the options that may be
    # safely set by the user can be changed in file dtr_user_settings.txt
    image_config = {
        # USER SETTINGS
        'image_x': -1,                # image size, x axis
        'image_y': -1,                # image size, y axis
        'seed': 0,                    # Blender cycles noise seed
        'frame': 1,                   # frame number to be rendered
        'blocks_user': 0,             # give the user the ability to raise the number of blocks
        'textures_directory': '',     # textures associated with the render file
        'library_directory': '',      # library blender files associated with the render file
        'auto_backup': '',            # back up in-progress render to another machine using rsync
        'remove_alpha': False,        # remove alpha channel from final image
        'render_order': 'CENTRE',     # the order the blocks are rendered in
        'spatial_splits': False,      # use spatial splits: longer build time, faster render
        'heatmap': False,             # flag indicating whether to generate a heatmap post render
        'filetype': 'OPEN_EXR',       # filetype to render to

        # RESERVED SETTINGS
        'blocks_x': -1,               # number of blocks, x axis
        'blocks_y': -1,               # number of blocks, y axis
        'blocks_required': -1,        # total number of blocks required (blocks_x * blocks_y)
        'padding': -1,                # size of block numbers field
        'tile_size_x': 16,            # x axis size of Blender tile in pixels
        'tile_size_y': 16,            # y axis size of Blender tile in pixels
        'min_tiles_per_block': 16,    # n tiles per block, tile = tile_size_x * tile_size_y pixels
        'max_threads': 8,             # = number of cores (for cpus without SMT),
                                      #     = number of cores * 2 (for cpus with SMT)
                                      #     the value used should be the highest obtained
                                      #     from all the render nodes used
        'max_blocks': 1024            # when the image to be rendered is very large,
                                      #     limit the number of blocks to something sane
    }

    # check any command line arguments supplied by the user
    cl_args = init.check_arguments(image_config)

    # check if we are restarting from an earlier interrupted render
    if os.path.isfile(ds.FILENAME_RESTART_CONFIG) and os.path.isfile(ds.FILENAME_RESTART_PROGRESS):
        available_render_nodes, image_config, blocks_to_render, checked, \
            time_taken_by_previous_renders = init.restart_interrupted_render(cl_args)
    else:
        available_render_nodes = []
        checked = []
        blocks_to_render, time_taken_by_previous_renders = \
            init.normal_start(available_render_nodes, image_config)

    # settings have now been established, process deferred command line option
    if cl_args.map:
        image_config['heatmap'] = True

    # let the user know which nodes are active, and what their performance is
    bm.display_bench_cache(available_render_nodes)

    ##########################################################################
    # render
    ##########################################################################

    init.display_render_details(available_render_nodes, image_config)

    print('>> rendering')

    # define data structures for processes to use
    rpipe = ds.Production()

    # fill queue with blocks to be rendered
    for block in blocks_to_render:
        rpipe.blocks.put(block)

    # start up processes
    parfun, info = start_processes(available_render_nodes, image_config, rpipe)

    # main loop - check that all the block transfers have completed
    user_action = ds.UserExit()
    while len(checked) < image_config['blocks_required'] and not user_action.interrupt:
        try:
            block_info = rpipe.check.get(timeout=1)
        except queue.Empty:
            continue
        else:
            checked.append(block_info)
            fio.progress_write((available_render_nodes, checked,
                                time_taken_by_previous_renders
                                + time.time() - script_start))
            fio.backup_render(image_config)

    # shut down processes
    stop_processes(rpipe, parfun, info)

    if user_action.interrupt:
        sys.exit('\nexiting: blender instances may still be running on render nodes')

    success = fio.create_final_image_from_blocks(image_config)
    if not success:
        success = fio.create_final_image_from_blocks(image_config, low_memory=True)

    ana.create_heatmap(image_config, checked)
    fio.backup_render(image_config)

    if success:
        fio.tidy_up_temporary_files(available_render_nodes, image_config)

    ana.display_basic_render_info(script_start, time_taken_by_previous_renders, checked)


##############################################################################
if __name__ == '__main__':

    # exit if the version of Python running this script is too old
    utils.check_python_version()

    # proceed with distributed render
    main()
