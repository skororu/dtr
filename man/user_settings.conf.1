.lf 1 stdin
.TH USER_SETTINGS.CONF 1 "2016/08/31" "DTR 0.148.5"
.\" Copyright 2015-2016 Alan Taylor
.\" 
.\" This file is part of dtr.
.\" 
.\" dtr is free software: you can redistribute it and/or modify
.\" it under the terms of the GNU General Public License as published by
.\" the Free Software Foundation, either version 3 of the License, or
.\" (at your option) any later version.
.\" 
.\" dtr is distributed in the hope that it will be useful,
.\" but WITHOUT ANY WARRANTY; without even the implied warranty of
.\" MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.\" GNU General Public License for more details.
.\" 
.\" You should have received a copy of the GNU General Public License
.\" along with dtr.  If not, see <http://www.gnu.org/licenses/>.
.\" 
.\" $DTR$
.SH NAME
user_settings.conf \- configuration file for dtr, distributed single frame render for Blender Cycles
.SH SYNOPSIS
user_settings.conf
.SH DESCRIPTION
The file
.B user_settings.conf
contains all user configuration settings for
.BR dtr (1).
.LP
The
.B user_settings.conf
file consists of a series of configuration options that control the process
of rendering a single frame over many computers. It is read when
.B dtr
is run to commence a new distributed render. If the previous render was
interrupted before it completed, and
.B dtr
is executed again to restart the render, the file will not be read again.
.LP
The configuration options are case-sensitive. Whitespace around the
equals sign is acceptable or can be omitted. There are no restrictions
on the order that options appear in.
.LP
The general format of
.B user_settings.conf
is as follows:
.LP
.nf
    # comment - any use of a hash (#) anywhere on a line marks the line
    as a comment
    <option> = <value>
    ...
.fi
.LP
Comments are optional.
.SH OPTIONS
.TP
.B auto_backup = <string value>
When this option is set, once a block has completed rendering and the server
has fetched it, the script will backup the directory it is being run from.
So if disaster strikes (e.g. the power goes out, and you were using an F2FS
filesystem on a USB memory stick) you can restore the whole directory to the 
server and simply restart the render.

As the script backs up the current working directory, MAKE SURE THE
SCRIPT IS INSTALLED IN A DIRECTORY OF ITS OWN, and you are in that
directory when you run the script.  i.e. don't dump the script in ~ and try
to run it from there, as it will back up your entire home directory.

Example: if we are running the script from /home/pi/usb/render
then setting auto_backup as follows:

auto_backup = gfx@192.168.0.128:~/Documents/design/blender

Would cause the script to run:

rsync -acq /home/pi/usb/render gfx@192.168.0.128:~/Documents/design/blender

If you are using a directory name with spaces, it doesn't matter if you
escape them or not, the script will handle this correctly.
If this option is omitted, no backups will be made, which is the default.
.TP
.B blocks_user = <integer value>
This is essentially a debugging option.
The script calculates the minimum number of blocks to divide the
image into for an efficient render, based on its assessment of the
relative performance of the user supplied render nodes, and compares
that value to
.B blocks_user,
it will then use the highest of the two values to
decide how many blocks to split the image into.

If this option is omitted, then the script will simply use the the minimum
value it calculates.
.TP
.B frame = <integer value>
The frame number to render. If this configuration
option is omitted, the script will render frame 1.
.TP
.B image_x = <integer value>
The horizontal resolution in pixels of the image to be rendered. If this configuration
option is omitted, the script will set a value of 3840 pixels.
.TP
.B image_y = <integer value>
The vertical resolution in pixels of the image to be rendered. If this configuration
option is omitted, the script will set a value of 2160 pixels.
.TP
.B library_directory = <string value>
The name of directory containing library Blender files needed for the render.

This directory must be in the same directory as the script itself.
If this option is omitted, no attempt will be made to copy any library files
to the remote render nodes.
.TP
.B node = <string value>
This option may be given multiple times, on separate lines, with each line
defining a unique render node. The render node may be specified as an IP
address or as a locally recognised hostname. Such as:

.B node = 10.0.0.15
.br
.B node = poppy

If just the IP address or hostname is given, it will be assumed that the username
to connect with is
.B render.
If you need to specify another username, use the following form:

.B node = alice@10.0.0.17
.br
.B node = bob@lupin
.TP
.B remove_alpha = <boolean value>
As the script renders blocks as RGBA, the final image once stitched
together will retain the alpha channel inherited from the blocks.
Most renders will not need the alpha channel present in the final image
and users can manually set
.B remove_alpha = True
to remove it, reducing the file size.

If this option is omitted, the value is set to
.B False
by default and the alpha channel will be left in place.
.TP
.B render_order = <string value>
This option specifies the order in which the blocks are despatched to the
render nodes, and is a purely cosmetic exercise, with no effect on
performance. If this option is omitted the default value used is
.B CENTRE
which renders the blocks from the centre of the image outwards in a circular
pattern. The other option available is
.B BOTTOM_TO_TOP
which renders from left to right, and from bottom to top.
.TP
.B seed = <integer value>
The Blender Cycles noise seed. If this configuration
option is omitted, the script will set a value of 0.
.TP
.B spatial_splits = <boolean value>
As the scene will be built each time a new block is rendered, enabling
spatial splits can add substantial additional overhead (much more so than
with a conventional render on a single computer), hence the default
value is False.  For particularly long renders, there is still value in
enabling it, but test it first.
.TP
.B textures_directory = <string value>
If you are using older versions of Blender that do not allow Cycles to use
textures packed into the Blender file, or you have a workflow that prefers
to keep textures separately, the
.B textures_directory
value can be set to the name of the directory where the textures are located.

This directory must be in the same directory as the script itself.
If this option is omitted, no attempt will be made to copy any textures
to the remote render nodes.
.SH EXAMPLES
.LP
Here is a short example of a configuration file:
.LP
.RS
.nf
# set image resolution
image_x=256
image_y=256

# set noise seed
seed=237

# frame number to render
frame=7

# name of directory containing library Blender files
library_directory=master_lib

# enable automatic backups to given location
auto_backup = user@192.168.0.128:~/rbak

# remove alpha channel from final image
remove_alpha=True

# list of render nodes to use
node=10.0.0.64
node=james@10.0.0.68
node=jen@poppy
node=lupin
.SH FILES
.TP
user_settings.conf
default configuration file
.SH SEE ALSO
.BR dtr (1)
.br
.BR blender (1)
.LP
"dtr wiki" (https://gitlab.com/skororu/dtr/wikis/home)
.lf 2066 stdin
