"""
all functions involved with file i/o

callable functions from this file:

    backup_render
    benchmark_cache_read *
    benchmark_cache_write *
    config_read *
    config_write *
    delete_blocks
    distribute_files_to_node
    flush_cache
    progress_read *
    progress_write *
    read_user_options
    render_data_read *
    render_data_write *
    tidy_up_restart
    tidy_up_temporary_files
    verify_blocks_present

    (*) partial function wrappers

------------------------------------------------------------------------------
Copyright 2015-2017 Alan Taylor

This file is part of dtr.

dtr is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

dtr is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with dtr.  If not, see <http://www.gnu.org/licenses/>.
------------------------------------------------------------------------------
"""

import concurrent.futures as cf  # ThreadPoolExecutor
import functools                 # partial
import multiprocessing as mp     # cpu_count
import os                        # path.getsize, path.isdir, path.isfile
import pickle                    # dump, load
import subprocess                # DEVNULL, Popen
import sys                       # exit

import dtr_data_struct as ds     # FILENAME_BENCHMARK_CACHE,
                                 # FILENAME_CONFIG_BLENDER,
                                 # FILENAME_RESTART_CONFIG,
                                 # FILENAME_RESTART_PROGRESS,
                                 # FILENAME_USER_SETTINGS
import dtr_utils as utils        # render_block_normal, block_render_filename


##############################################################################
# command line options
##############################################################################

def flush_cache():
    """
    delete the benchmark cache

    --------------------------------------------------------------------------
    args : none
    --------------------------------------------------------------------------
    returns : none
    --------------------------------------------------------------------------
    """
    delete_files = ['rm', '-f', ds.FILENAME_BENCHMARK_CACHE]

    # do not check the response code
    with subprocess.Popen(delete_files,
                          stdout=subprocess.DEVNULL,
                          stderr=subprocess.DEVNULL) as proc:
        proc.wait()


def delete_blocks(settings, cl_args):
    """
    delete a series of blocks that fully contain the area given

    to obtain the values of min_x, max_x, min_y and max_y, the user should set a
    border area in the camera viewport, then in the console type:

    bpy.context.scene.render.border_min_x
    bpy.context.scene.render.border_max_x
    bpy.context.scene.render.border_min_y
    bpy.context.scene.render.border_max_y

    --------------------------------------------------------------------------
    args
        settings : image_config dictionary
            contains core information about the image to be rendered
        cl_args : list [min_x, max_x, min_y, max_y]
            where:
            min_x : float, value between 0.0 and 1.0
            max_x : float, value between 0.0 and 1.0
            min_y : float, value between 0.0 and 1.0
            max_y : float, value between 0.0 and 1.0
    --------------------------------------------------------------------------
    returns : none
    --------------------------------------------------------------------------
    """
    names = ['min_x', 'max_x', 'min_y', 'max_y']
    print('>> deleting blocks which contain the area',
          ', '.join(x[0] + ' ' + format(x[1], '.3f') for x in zip(names, cl_args)))

    for block in utils.area_in_blocks(settings, cl_args):
        filename = utils.block_render_filename(settings, block)
        if os.path.isfile(filename):
            with subprocess.Popen(['rm', '-f', filename],
                                  stdout=subprocess.DEVNULL,
                                  stderr=subprocess.DEVNULL) as proc:
                proc.wait()
            print('block', block, 'deleted')


##############################################################################
# read, validate and store user settings
##############################################################################

def _check_setting_int(settings, name, val):
    """
    check for render settings with simple integer values

    provides a service to read_user_options()
    """
    if val.isdigit():
        settings[name] = int(val)
    else:
        print(name + '=' + val + ' (invalid: positive integer expected)')


def _check_setting_bool(settings, name, val):
    """
    check for render settings with boolean values

    provides a service to read_user_options()
    """
    if val.lower() == 'true':
        settings[name] = True
    elif val.lower() == 'false':
        settings[name] = False
    else:
        print(name + '=' + val + ' (invalid: boolean value True/False expected)')


def _check_setting_directory(settings, name, val):
    """
    check if the directory exists

    provides a service to read_user_options()
    """
    if os.path.isdir(val):
        settings[name] = val
        print(name, 'set to', val)
    else:
        print(name + '=' + val + ' (invalid: directory does not exist)')


def _check_setting_backup(settings, name, val):
    """
    test to see if we can perform a backup to the target machine
    if it fails the script will report the error and then exit

    provides a service to read_user_options()
    """
    print('>> testing backup')
    settings[name] = val
    backup_render(settings, True)


def _check_setting_order(settings, name, val):
    """
    the order tiles are despatched to render nodes

    provides a service to read_user_options()
    """
    if val in ['CENTRE', 'BOTTOM_TO_TOP']:
        settings[name] = val


def _check_setting_filetype(settings, name, val):
    """
    the filetype to render to

    provides a service to read_user_options()
    """
    if val in ['PNG', 'OPEN_EXR']:
        settings[name] = val


def read_user_options(render_nodes, settings):
    """
    read user set render options from a configuration file on disk
    and perform some basic validation checks

    it is possible for the user to have set the same values multiple
    times, the last value set will be the one that is used.

    ignore any line with a hash (#) *anywhere*

    use the check_setting dictionary to call a function appropriate to the
    variable name the user is trying to set this performs some basic checks to
    see the supplied value is reasonable

    --------------------------------------------------------------------------
    args
        render_nodes : list of RenderNode instances
            contains information about all the nodes in the cluster
        settings : image_config dictionary
            contains core information about the image to be rendered
    --------------------------------------------------------------------------
    returns
        render_nodes : list of RenderNode instances
            mutable type amended in place, no explicit return
        settings : image_config dictionary
            mutable type amended in place, no explicit return
    --------------------------------------------------------------------------
    """
    check_setting = {
        'image_x': _check_setting_int,
        'image_y': _check_setting_int,
        'seed': _check_setting_int,
        'frame': _check_setting_int,
        'filetype': _check_setting_filetype,
        'blocks_user': _check_setting_int,
        'textures_directory': _check_setting_directory,
        'library_directory': _check_setting_directory,
        'auto_backup': _check_setting_backup,
        'render_order': _check_setting_order,
        'remove_alpha': _check_setting_bool,
        'spatial_splits': _check_setting_bool
    }

    print('>> checking user settings file')
    if not os.path.isfile(ds.FILENAME_USER_SETTINGS):
        sys.exit('user settings file ' + ds.FILENAME_USER_SETTINGS + ' not found: exiting')

    with open(ds.FILENAME_USER_SETTINGS) as user_settings_file:
        for line in user_settings_file:
            # expected format is variable=value
            # remove whitespace from around each part
            str_tidy = [x.strip() for x in line.rpartition('=')]
            assignment_malformed = '' in str_tidy
            line_is_a_comment = '#' in line

            if line_is_a_comment or assignment_malformed:
                continue

            # this looks like a plausible user setting, unpack into parts
            var_name, _, value = str_tidy

            if var_name == 'node':
                # user is setting a render node ip address or host name

                # check if we have just the ip address or host name, or
                # if we have something of the form: username@ip_address
                username, _, ip_hostname = value.rpartition('@')
                if ip_hostname:
                    render_nodes.append(ds.RenderNode(ip_hostname))
                    if username:
                        render_nodes[-1].username = username
            else:
                # check line for other recognised settings
                # and set them if their values are reasonable
                if check_setting.get(var_name):
                    check_setting[var_name](settings, var_name, value)

    # check if the user has set both x and y axes
    if settings['image_x'] == -1 or settings['image_y'] == -1:
        sys.exit('both image_x and image_y need to be set in ' + ds.FILENAME_USER_SETTINGS)


##############################################################################
# confidence check on restart
##############################################################################

def verify_blocks_present(settings, blocks_yet_to_render):
    """
    check we have block renders on disk for each block marked as already
    completed return a set of any missing blocks that need to be rendered again

    --------------------------------------------------------------------------
    args
        settings : image_config dictionary
            contains core information about the image to be rendered
        blocks_yet_to_render : list of unique ints
            contains blocks that have either yet to be issued for rendering
            or, have been issued and are still rendering
    --------------------------------------------------------------------------
    returns
        missing_blocks : set of ints
            a set of blocks that need to be rendered again
    --------------------------------------------------------------------------
    """
    set_of_all_blocks = set(range(1, settings['blocks_required'] + 1))
    set_of_rendered_blocks = set_of_all_blocks - set(blocks_yet_to_render)

    missing_blocks = set()
    for block in set_of_rendered_blocks:
        missing_or_empty = False

        try:
            filesize = os.path.getsize(utils.block_render_filename(settings, block))
        except OSError:
            missing_or_empty = True
        else:
            if filesize == 0:
                missing_or_empty = True

        if missing_or_empty:
            missing_blocks.add(block)

    return missing_blocks


##############################################################################
# manage temporary files used during the render
##############################################################################

def _data_write(data, filename):
    """
    store data to file for later retrieval

    this function is internal to this module, and is not called directly
    see partial function wrappers below (which are callable)

    --------------------------------------------------------------------------
    args
        data : datatype, or datatypes wrapped in a tuple
        filename : string
    --------------------------------------------------------------------------
    returns : none
    --------------------------------------------------------------------------
    """
    with open(filename, 'wb') as outfile:
        # use data stream format protocol 4, for Python 3.4 or later
        pickle.dump(data, outfile, 4)


# wrappers for _data_write
benchmark_cache_write = functools.partial(_data_write, filename=ds.FILENAME_BENCHMARK_CACHE)
config_write = functools.partial(_data_write, filename=ds.FILENAME_RESTART_CONFIG)
progress_write = functools.partial(_data_write, filename=ds.FILENAME_RESTART_PROGRESS)
render_data_write = functools.partial(_data_write, filename=ds.FILENAME_RENDER_DATA)


def _data_read(filename):
    """
    retrieve previously stored data from file

    this function is internal to this module, and is not called directly
    see partial function wrappers below (which are callable)

    --------------------------------------------------------------------------
    args
        filename : string
    --------------------------------------------------------------------------
    returns
        restored_progress : tuple (x, y, z)
            where:
            x = list of ints,
            y = list of RenderNode instances,
            z = float)
            set of blocks for restart, available render nodes, time taken by
            previous renders
    --------------------------------------------------------------------------
    """
    with open(filename, 'rb') as infile:
        try:
            restored_progress = pickle.load(infile)
        except (pickle.UnpicklingError, AttributeError, EOFError, ImportError, IndexError):
            sys.exit('exiting: problem reading data from ' + filename)
        else:
            return restored_progress


# wrappers for _data_read
benchmark_cache_read = functools.partial(_data_read, filename=ds.FILENAME_BENCHMARK_CACHE)
config_read = functools.partial(_data_read, filename=ds.FILENAME_RESTART_CONFIG)
progress_read = functools.partial(_data_read, filename=ds.FILENAME_RESTART_PROGRESS)
render_data_read = functools.partial(_data_read, filename=ds.FILENAME_RENDER_DATA)


##############################################################################
# distribute files to render nodes
#
#    * blender file
#        any supporting blender library files
#        any supporting textures
#    * python script to configure blender to render the block required
#
##############################################################################

def filetype_defaults(settings):
    """
    generate configuration values for user specified render filetype

    --------------------------------------------------------------------------
    args
        settings : image_config dictionary
            contains core information about the image to be rendered
    --------------------------------------------------------------------------
    returns : list
    --------------------------------------------------------------------------
    """
    ft_defaults = {
        'OPEN_EXR': ['scene.render.image_settings.file_format="OPEN_EXR"',
                     'scene.render.image_settings.color_depth="16"',
                     'scene.render.image_settings.color_mode="RGBA"',
                     'scene.render.image_settings.exr_codec="ZIP"'],
        'PNG': ['scene.render.image_settings.file_format="PNG"',
                'scene.render.image_settings.color_mode="RGBA"',
                'scene.render.image_settings.compression=100']}

    config = ft_defaults.get(settings['filetype'], None)
    assert config is not None, 'ASSERT: unrecognised filetype in filetype_defaults'

    return config


def _write_blender_python_config(settings, block_number):
    """
    write configuration python script to instruct blender to render a single
    block using border rendering

    assert sensible defaults to minimise render time in the context of a
    distributed render

    --------------------------------------------------------------------------
    args
        settings : image_config dictionary
            contains core information about the image to be rendered
        block_number : int
            block number to render
    --------------------------------------------------------------------------
    returns : none
    --------------------------------------------------------------------------
    """
    block_coords = utils.render_block_normal(settings, block_number)
    part1 = [
        'import bpy',
        'scene=bpy.context.scene',
        # enable border rendering so we can define the area of the block to be
        # rendered
        'scene.render.use_border=True',
        # set the output filename
        'scene.render.filepath="//' + utils.block_render_filename(settings, block_number) + '"',
        # set the noise seed
        'scene.cycles.seed=' + str(settings['seed']),
        # the default on blender versions earlier than 2.77 is to render from the
        # centre outwards - which is fine for viewport renders - but
        # is slightly slower than methods that render adjacent tiles in order
        'scene.cycles.tile_order="BOTTOM_TO_TOP"',
        # tends to give small performance improvements for renders where
        # block render time >> scene build time
        'scene.cycles.debug_use_spatial_splits=' + str(settings['spatial_splits']),
        # make sure progressive refine is disabled
        'scene.cycles.use_progressive_refine=False',
        # avoid unnecessary file writes
        'scene.render.use_save_buffers=False',
        # set number of threads to automatic
        'scene.render.threads_mode="AUTO"',
        # set the block boundaries
        'scene.render.border_min_x=' + str(block_coords[0]),
        'scene.render.border_min_y=' + str(block_coords[1]),
        'scene.render.border_max_x=' + str(block_coords[2]),
        'scene.render.border_max_y=' + str(block_coords[3]),
        # set the tile size for Blender to that used by the script for
        # calculating the optimal block arrangement
        'scene.render.tile_x=' + str(settings['tile_size_x']),
        'scene.render.tile_y=' + str(settings['tile_size_y']),
        # set the image resolution
        'scene.render.resolution_x=' + str(settings['image_x']),
        'scene.render.resolution_y=' + str(settings['image_y']),
        'scene.render.resolution_percentage=100',
        # set frame
        'scene.frame_set(' + str(settings['frame']) + ')',
        # post processing should always be suppressed for block renders
        'scene.render.use_compositing=False',
        'scene.render.use_sequencer=False']
    part2 = filetype_defaults(settings)
    part3 = [
        # instruction to render the image
        'bpy.ops.render.render(write_still=True)']
    commands = '\n'.join(part1 + part2 + part3)

    filename = ds.FILENAME_CONFIG_BLENDER + str(block_number).zfill(settings['padding']) + '.py'
    with open(filename, 'w') as configuration:
        configuration.write(commands + '\n')


def distribute_files_to_node(settings, node, rt_block):
    """
    before initiating the asynchronous render, make sure the node has all the
    files it needs

    rsync options used:
    -a    archive mode
    -c    compare files based on checksum
    -q    quiet

    --------------------------------------------------------------------------
    args
        settings : image_config dictionary
            contains core information about the image to be rendered
        node : instance of RenderNode
            contains information about the node we will send a block to
        rt_block : int
           the number of the block to be rendered
    --------------------------------------------------------------------------
    returns : none
    --------------------------------------------------------------------------
    """
    # write a blender config file and copy to the node
    _write_blender_python_config(settings, rt_block)
    local_filename = ds.FILENAME_CONFIG_BLENDER + str(rt_block).zfill(settings['padding']) + '.py'
    remote_filename = node.username + '@' + node.ip_address + ':~/' + local_filename
    transfer_files = ' '.join(['rsync -acq', local_filename, remote_filename])
    with subprocess.Popen(transfer_files, shell=True,
                          stdout=subprocess.DEVNULL,
                          stderr=subprocess.DEVNULL) as proc:
        proc.wait()

    # if this is the first block the node has received, send the render files
    # too
    if node.first_use:
        remote_filename = node.username + '@' + node.ip_address + ':~'

        # render file
        with subprocess.Popen(['rsync', '-acq', 'render.blend', remote_filename],
                              stdout=subprocess.DEVNULL,
                              stderr=subprocess.DEVNULL) as proc:
            proc.wait()

        # textures directory
        if os.path.isdir(settings['textures_directory']):
            with subprocess.Popen(['rsync', '-acq',
                                   settings['textures_directory'],
                                   remote_filename],
                                  stdout=subprocess.DEVNULL,
                                  stderr=subprocess.DEVNULL) as proc:
                proc.wait()

        # library directory
        if os.path.isdir(settings['library_directory']):
            with subprocess.Popen(['rsync', '-acq',
                                   settings['library_directory'],
                                   remote_filename],
                                  stdout=subprocess.DEVNULL,
                                  stderr=subprocess.DEVNULL) as proc:
                proc.wait()

        node.first_use = False


##############################################################################
# automatic backup of in-progress render
##############################################################################

def backup_render(settings, initial_test=False):
    """
    backup the render directory to a user specified location

    typically the destination held in settings['auto_backup'] would be either
    the default '' (do not backup), or a user setting, usually in the
    form of user@host:directory

    if we are performing the initial test backup (initial_test=True) add
    --delete to the rsync options so we clear out the blocks from the previous
    render, to avoid confusion

    rsync options:
    a    --archive     archive mode
    c    --checksum    skip based on checksum, not mod-time & size
    q    --quiet       suppress non-error messages
         --delete      delete files from the destination directory that are
                       not present in source directory
    --------------------------------------------------------------------------
    args
        settings : image_config dictionary
            contains core information about the image to be rendered
        initial_test : bool
            flag to indicate if this is the initial test backup after reading
            the user configuration file, or a normal backup during the render
    --------------------------------------------------------------------------
    returns : none
    --------------------------------------------------------------------------
    """
    if settings['auto_backup']:
        init_del = ' --delete ' if initial_test else ''
        transfer_files = ' '.join(['rsync -acq', init_del, '"$PWD"', '"'
                                   + settings['auto_backup'] + '"'])

        # the shell is needed to parse $PWD
        with subprocess.Popen(transfer_files, shell=True,
                              stdout=subprocess.DEVNULL,
                              stderr=subprocess.DEVNULL) as proc:
            transfer_failed = proc.wait()

        if transfer_failed:
            print('automatic backup failed, rsync returned ' + str(transfer_failed))

            # only terminate the script if this is the initial test
            # otherwise the script should continue to manage the rendering process
            if initial_test:
                sys.exit('initial backup test failed, exiting')


##############################################################################
# final composite
##############################################################################

def create_final_image_from_blocks(settings, low_memory=False):
    """
    combine the rendered blocks together to form the final image

    --------------------------------------------------------------------------
    args
        settings : image_config dictionary
            contains core information about the image to be rendered
    --------------------------------------------------------------------------
    returns
        retval : boolean
            True if final image was created successfully, False otherwise
    --------------------------------------------------------------------------
    """
    retval = False

    print('>> compositing')

    extension = utils.file_extension(settings)
    filename = 'composite_seed_' + str(settings['seed']) + '.' + extension
    memory = '-limit memory 256MB -limit map 512MB ' if low_memory else ''
    composite = 'convert ' + memory \
                + '-layers flatten -compress zip block_*_seed_*.' \
                + extension + ' ' + filename

    # the shell is needed to parse the wildcards
    with subprocess.Popen(composite, shell=True,
                          stdout=subprocess.DEVNULL,
                          stderr=subprocess.DEVNULL) as proc:
        proc.wait()

    if proc.returncode == 0:
        if settings['remove_alpha']:
            strip_alpha = 'convert -alpha off -compress zip ' + filename + ' ' + filename

            # the shell is needed to parse the wildcards
            with subprocess.Popen(strip_alpha, shell=True,
                                  stdout=subprocess.DEVNULL,
                                  stderr=subprocess.DEVNULL) as proc:
                proc.wait()

        print('>> result written to: ' + filename)
        retval = True
    else:
        print('>> writing to: ' + filename + ' failed')

    return retval


##############################################################################
# tidy up
##############################################################################

def tidy_up_restart(settings, full_clear=False):
    """
    on the local machine, remove temporary files associated with a
    successfully completed, or previously interrupted render

    --------------------------------------------------------------------------
    args
        settings : image_config dictionary
            contains core information about the image to be rendered
        full_clear : bool
            flag to indicate whether to clear heatmap related data
    --------------------------------------------------------------------------
    returns : none
    --------------------------------------------------------------------------
    """
    command = ['rm -f',
               'block_*_seed_*.' + utils.file_extension(settings),
               ds.FILENAME_CONFIG_BLENDER + '*.py',
               ds.FILENAME_RESTART_CONFIG,
               ds.FILENAME_RESTART_PROGRESS]

    if full_clear:
        command.append(ds.FILENAME_RENDER_DATA)

    delete_files = ' '.join(command)

    # the shell is needed to parse the wildcards
    with subprocess.Popen(delete_files, shell=True,
                          stdout=subprocess.DEVNULL,
                          stderr=subprocess.DEVNULL) as proc:
        proc.wait()


def _tidy_files(settings, node):
    """
    tidy up files on a remote node

    as users may be performing multiple renders of the same Blender file
    we should avoid the temptation to remove the Blender file and any
    associated textures or library files, in the interest of limiting wear
    on flash based media.  so just remove python configuration files,
    rendered blocks, and image renders from benchmarking

    run asynchronously from tidy_up_temporary_files()

    --------------------------------------------------------------------------
    args
        settings : image_config dictionary
            contains core information about the image to be rendered
        node : instance of RenderNode
            contains information about the node we will send a block to
    --------------------------------------------------------------------------
    returns
        tuple x, y
        where:
        x = int, return code from command execution
        y = string, ip address on which the command was executed
    --------------------------------------------------------------------------
    """
    on_this_node = node.username + '@' + node.ip_address
    delete_files = ' '.join([
        'rm -f',
        'block_*_seed_*.' + utils.file_extension(settings),
        '0001.png',
        ds.FILENAME_CONFIG_BLENDER + '*.py'])

    with subprocess.Popen(['ssh', on_this_node, delete_files],
                          stdout=subprocess.DEVNULL,
                          stderr=subprocess.DEVNULL) as proc:
        resp = proc.wait()

    return resp, node.ip_address


def tidy_up_temporary_files(nodes, settings):
    """
    remove temporary files on local machine and remote nodes

    with regard to the default value of max_workers for
    concurrent.futures.ThreadPoolExecutor:
    set the value of max_workers manually to make sure we get the
    latest behaviour when using Python 3.4

    --------------------------------------------------------------------------
    args
        nodes : list of RenderNode instances
            contains information about all the nodes in the cluster
        settings : image_config dictionary
            contains core information about the image to be rendered
    --------------------------------------------------------------------------
    returns : none
    --------------------------------------------------------------------------
    """
    print('>> tidying up')

    # tidy up remote nodes
    try:
        num_threads = mp.cpu_count() * 5
    except NotImplementedError:
        num_threads = 5

    with cf.ThreadPoolExecutor(max_workers=num_threads) as executor:
        delete_status = {executor.submit(_tidy_files, settings, node): node for node in nodes}
        for status in cf.as_completed(delete_status):
            tidy_failed, node_ip = status.result()
            if tidy_failed:
                print('problem deleting temporary files on', node_ip)

    # tidy up locally
    tidy_up_restart(settings)

    print('>> finished')
